package com.evenmo.my_demo.pojo;

import lombok.Data;

import java.io.Serializable;
/**
* @author: moyongjun
* @Date: 2019-08-22 20:36
* @Description: 阻止序列化测试
*
*/
@Data
public class User implements Serializable {
    private String id;
    private String username;
    //密码，放弃序列化，对数据库写入此字段无影响，对写入磁盘文件有效
    private transient String password;
}
