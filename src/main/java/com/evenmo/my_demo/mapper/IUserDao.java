package com.evenmo.my_demo.mapper;

import com.evenmo.my_demo.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface IUserDao {
   @Select("select * from test_user")
    public List<User> queryAllUsers();
    @Insert("insert into test_user(username,password) values(#{username},#{password})")
    public void addUser(User user);
}
