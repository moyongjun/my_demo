package com.evenmo.my_demo;

import com.evenmo.my_demo.mapper.IUserDao;
import com.evenmo.my_demo.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyDemoApplicationTests {
@Autowired
    private IUserDao userDao;
    @Test
    public void contextLoads() {
        System.out.println("hello world!");
    }
@Test
    public void  addUser(){
        User user= new User();
        user.setPassword("123123");
        user.setUsername("testuser");
    System.out.println(user);
    userDao.addUser(user);
    }
    @Test
    public void queryUser(){
        List<User> users = userDao.queryAllUsers();

        if (users!=null){
            for (User user : users) {
                System.out.println(user);
            }
        }

    }
}
